package com.example.weatherbroadcast;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.List;

import static com.example.weatherbroadcast.models.MyResponse.Data.Weather;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.Viewholder> {

    interface WeatherClickListener {
        void onClick(Weather weather);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private List<Weather> weathers;
    private WeatherClickListener weatherClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            weatherClickListener.onClick(weathers.get(position));
        }
    };

    private Context context;

    public WeatherAdapter(List<Weather> weathers, Context context, WeatherClickListener weatherClickListener) {
        this.weathers = weathers;
        this.context = context;
        this.weatherClickListener = weatherClickListener;
    }

    @Override
    public WeatherAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.home, parent, false);
        return new Viewholder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {

        Weather weather = weathers.get(position);

        Picasso.
                with(context).
                load(weather.hourly.get(0).weatherIconUrl.get(0).value).
                into(holder.icon_weather);

        holder.date.setText(weather.date);

        holder.weatherDesc.setText(weather.hourly.get(0).weatherDesc.get(0).value);
        holder.tempCText.setText(weather.hourly.get(0).tempC);
        holder.FeelsLikeCText.setText(weather.hourly.get(0).FeelsLikeC);
        holder.pressureText.setText(weather.hourly.get(0).pressure);

        holder.tempCImg.setImageResource(R.mipmap.thermometer);
        holder.FeelsLikeCImg.setImageResource(R.mipmap.feel_weather);
        holder.pressureImg.setImageResource(R.mipmap.davlenye);
    }

    @Override
    public int getItemCount() {
        return weathers.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView icon_weather;
        TextView date, weatherDesc, tempCText, FeelsLikeCText, pressureText;
        ImageView tempCImg, FeelsLikeCImg, pressureImg;

        ItemClickListener itemClickListener;

        public Viewholder(View itemView, final ItemClickListener itemClickListener) {
            super(itemView);

            this.itemClickListener = itemClickListener;

            icon_weather = (ImageView) itemView.findViewById(R.id.icon_weather);
            date = (TextView) itemView.findViewById(R.id.date);
            weatherDesc = (TextView) itemView.findViewById(R.id.weatherDesc);
            tempCImg = (ImageView) itemView.findViewById(R.id.temp_c_img);
            tempCText = (TextView) itemView.findViewById(R.id.temp_c_text);
            FeelsLikeCImg = (ImageView) itemView.findViewById(R.id.feels_like_c_img);
            FeelsLikeCText = (TextView) itemView.findViewById(R.id.feels_like_c_text);
            pressureImg = (ImageView) itemView.findViewById(R.id.pressure_img);
            pressureText = (TextView) itemView.findViewById(R.id.pressure_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
