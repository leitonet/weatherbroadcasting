package com.example.weatherbroadcast.models;

import java.io.Serializable;
import java.util.List;

public class MyResponse implements Serializable{

    public Data data;

    public static class Data implements Serializable{

        public List<Request> request = null;
        //        public List<CurrentCondition> currentCondition = null;
        public List<Weather> weather = null;

        public static class Request implements Serializable{

            public String type; //city
            public String query; // Kharkiv, Ukraine

        }

        public static class CurrentCondition implements Serializable{

            public String pressure;
            public String tempC;
            public String feelsLikeC;
            public String tempF;
            public String windspeedKmph;
        }

        public static class Weather implements Serializable{

            public String date;
            public List<Astronomy> astronomy = null;
            public List<Hourly> hourly = null;

            public static class Astronomy implements Serializable{

                public String sunrise;
                public String sunset;
                public String moonrise;
                public String moonset;
            }

            public static class Hourly implements Serializable{

                public String pressure;
                public String tempC;
                public String FeelsLikeC;
                public String tempF;
                public String windspeedKmph;
                public String chanceofrain;
                public String chanceofhightemp;
                public String chanceofsnow;
                public String chanceofwindy;
                public List<WeatherDesc> weatherDesc = null;
                public List<WeatherIconUrl> weatherIconUrl = null;

                public static class WeatherDesc implements Serializable{

                    public String value;
                }

                public static class WeatherIconUrl implements Serializable{

                    public String value;
                }
            }
        }
    }
}
