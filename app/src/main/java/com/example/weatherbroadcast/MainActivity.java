package com.example.weatherbroadcast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.weatherbroadcast.models.MyResponse;
import com.example.weatherbroadcast.models.MyResponse.Data.Weather;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements WeatherAdapter.WeatherClickListener {

    public static final String KEY = "weather";
    RecyclerView rList;
    WeatherAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rList = (RecyclerView) findViewById(R.id.list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rList.setLayoutManager(layoutManager);

        Retrofit.getData(new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
                Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_LONG).show();

                if(myResponse != null) {
                    adapter = new WeatherAdapter(myResponse.data.weather, MainActivity.this, MainActivity.this);
                    rList.setAdapter(adapter);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(Weather weather) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(KEY, weather);
        startActivity(intent);
    }
}
