package com.example.weatherbroadcast;

import com.example.weatherbroadcast.models.MyResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class Retrofit {
    private static final String ENDPOINT = "http://api.worldweatheronline.com";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/premium/v1/weather.ashx?key=986f909fafee4cec8de73206170107&q=Kharkiv&format=json&num_of_days=5&tp=24")
        void getData(Callback<MyResponse> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }


    public static void getData (Callback<MyResponse> callback) {
        apiInterface.getData(callback);
    }
}

