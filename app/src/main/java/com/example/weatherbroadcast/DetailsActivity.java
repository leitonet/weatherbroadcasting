package com.example.weatherbroadcast;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.weatherbroadcast.models.MyResponse.Data.Weather;

public class DetailsActivity extends AppCompatActivity {

    public static final String KEY = "weather";
    TextView dateTextview, temp_C2_text, feelsLikeC2_text, pressure2_text, descriptionTextview, wind_text, sunrise_text, sunset_text,
            moon_start_text, moon_end_text, descTwoTextView, forecast_text1, forecast_text2, forecast_text3, forecast_text4;
    ImageView temp_C2_img, FeelsLikeC2_img, pressure2_img, wind_img, sunrise_img, sunset_img, moon_start_img, moon_end_img, forecast_img1,
            forecast_img2, forecast_img3, forecast_img4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        dateTextview = (TextView) findViewById(R.id.dateTextview);
        temp_C2_text = (TextView) findViewById(R.id.temp_C2_text);
        feelsLikeC2_text = (TextView) findViewById(R.id.FeelsLikeC2_text);
        pressure2_text = (TextView) findViewById(R.id.pressure2_text);
        descriptionTextview = (TextView) findViewById(R.id.descriptionTextview);
        wind_text = (TextView) findViewById(R.id.wind_text);
        sunrise_text = (TextView) findViewById(R.id.sunrise_text);
        sunset_text = (TextView) findViewById(R.id.sunset_text);
        moon_start_text = (TextView) findViewById(R.id.moon_start_text);
        moon_end_text = (TextView) findViewById(R.id.moon_end_text);
        forecast_text1 = (TextView) findViewById(R.id.forecast_text1);
        forecast_text2 = (TextView) findViewById(R.id.forecast_text2);
        forecast_text3 = (TextView) findViewById(R.id.forecast_text3);
        forecast_text4 = (TextView) findViewById(R.id.forecast_text4);

        temp_C2_img = (ImageView) findViewById(R.id.temp_C2_img);
        FeelsLikeC2_img = (ImageView) findViewById(R.id.FeelsLikeC2_img);
        pressure2_img = (ImageView) findViewById(R.id.pressure2_img);
        wind_img = (ImageView) findViewById(R.id.wind_img);
        sunrise_img = (ImageView) findViewById(R.id.sunrise_img);
        sunset_img = (ImageView) findViewById(R.id.sunset_img);
        moon_start_img = (ImageView) findViewById(R.id.moon_start_img);
        moon_end_img = (ImageView) findViewById(R.id.moon_end_img);
        forecast_img1 = (ImageView) findViewById(R.id.forecast_img1);
        forecast_img2 = (ImageView) findViewById(R.id.forecast_img2);
        forecast_img3 = (ImageView) findViewById(R.id.forecast_img3);
        forecast_img4 = (ImageView) findViewById(R.id.forecast_img4);
        forecast_img1.setImageResource(R.mipmap.rain_forecast);
        forecast_img2.setImageResource(R.mipmap.high_temperature1_forecast);
        forecast_img3.setImageResource(R.mipmap.wind1_forecast);
        forecast_img4.setImageResource(R.mipmap.snow1_forecast);

        if(getIntent()!= null){
            Weather weather = (Weather) getIntent().getSerializableExtra(KEY);
            dateTextview.setText(weather.date);
            temp_C2_text.setText(weather.hourly.get(0).tempC);
            feelsLikeC2_text.setText(weather.hourly.get(0).FeelsLikeC);
            pressure2_text.setText(weather.hourly.get(0).pressure);
            descriptionTextview.setText(weather.hourly.get(0).weatherDesc.get(0).value);
            wind_text.setText(weather.hourly.get(0).windspeedKmph);
            sunrise_text.setText(weather.astronomy.get(0).sunrise);
            sunset_text.setText(weather.astronomy.get(0).sunset);
            moon_start_text.setText(weather.astronomy.get(0).moonrise);
            moon_end_text.setText(weather.astronomy.get(0).moonset);
            forecast_text1.setText(weather.hourly.get(0).chanceofrain + "%");
            forecast_text2.setText(weather.hourly.get(0).chanceofhightemp + "%");
            forecast_text3.setText(weather.hourly.get(0).chanceofwindy + "%");
            forecast_text4.setText(weather.hourly.get(0).chanceofsnow + "%");
        }
    }
}
